`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    06:31:31 06/24/2014 
// Design Name: 
// Module Name:    input_controller 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module input_controller(
    input clk,
	 input reset,
    input ascii_ready,
    input [6:0] ascii_code,
	 input [6:0] read_data,
    output [6:0] write_data,
	 output [6:0] cur_x,
	 output [6:0] cur_y,
    output we,
    output [11:0] write_addr,
	 output bell,
	 output visible,
	 output rd_uart,
	 output [11:0] top_pointer
    );

   // constant declaration for ascii control codes
   localparam 	BELL = 7'h07,			// bell
					SPACE = 7'h20,			// space
					BACKSPACE = 7'h08,	// backspace
					DEL = 7'h7f,			// delete
					TAB = 7'h09,			// tab
					LF = 7'h0a, 			// line feed, new line
					CR = 7'h0d, 			// carriage return
					ESC = 7'h1b, 			// escape
					BRACKET = 7'h5b;		// [
					
   // symbolic state declaration
   localparam
		get_ascii_code = 5'b00000,
		save_ascii_to_ram_cfg = 5'b00001,
		save_ascii_to_ram = 5'b00010,
		do_backspace = 5'b00011,
		do_tidy = 5'b00100,
		do_tab = 5'b00101,
		do_lf = 5'b00110,
		do_cr = 5'b00111,
		do_esc = 5'b01000,
		do_reset = 5'b01001,
		clear_old_line = 5'b01010,
		get_bracket = 5'b01101,
		get_p1 = 5'b01110,
		get_p2 = 5'b01111,
		get_p3 = 5'b10000,
		get_p4 = 5'b10001,
		get_5 = 5'b10010,
		get_visible = 5'b10011,
		move_top_left = 5'b10100,
		clear_screen = 5'b10101,
		get_2 = 5'b10110,
		get_seperator = 5'b10111,
		get_H = 5'b11000,
		move_location = 5'b11001;

   // 80-by-30 tile map
   localparam MAX_X = 80;
   localparam MAX_Y = 30;	
	
   // cursor position
   reg [6:0] cur_x_reg = 0;
   reg [6:0] cur_x_next;
   reg [6:0] cur_y_reg = 0;
   reg [6:0] cur_y_next; 
	reg visible_reg = 1;
	reg visible_next;
	assign visible = visible_reg;
	reg [11:0] pos, pos_next;
	
	assign cur_x = cur_x_reg;
	assign cur_y = cur_y_reg;

	//state registers
	reg [4:0] state_reg, state_next;
	reg [6:0] current_code;
	reg [6:0] p1;
	reg [6:0] p2;
	reg [6:0] p3;
	reg [6:0] p4;
	
	
	
	//ram logic signals
	reg write_e = 1'b0;	
	assign we = write_e;
	reg [11:0] address = 12'h000;	
	//calculate address of cell.
	wire [11:0] real_address;
	reg [11:0] base_pointer = 12'h000;
	reg [11:0] base_pointer_next = 12'h000;
	assign real_address = base_pointer + address;
	assign top_pointer = base_pointer;
	//assign address and data signals
	assign write_addr = real_address;
	assign write_data = current_code;

	
	//bell
	reg do_bell = 1'b0;
	assign bell = do_bell;
	
	
	reg rd_uart_int;
	assign rd_uart = rd_uart_int;

	/////////////////////////
	/// BEHAVIOURAL
	/////////////////////////

	//=======================================================
   // FSM to process ascii codes
   //=======================================================
   // state registers
   always @(posedge clk, posedge reset)
      if (reset)
			begin
				state_reg <= do_reset;
				cur_x_reg <= 7'h00;
				cur_y_reg <= 7'h00;
				visible_reg <= 1'b1;
				pos <= 12'h000;
				base_pointer <= 12'h000;
			end
      else
			begin
				state_reg <= state_next;
				cur_x_reg <= cur_x_next;
				cur_y_reg <= cur_y_next;
				visible_reg <= visible_next;
				pos <= pos_next;
				base_pointer <= base_pointer_next;
			end


// next-state logic OUCH !
   always @(posedge clk, posedge reset)
	if (reset)
		begin
			current_code <= 7'b0000000;
			address <= 12'h000;
			p1 <= 7'h00;
			p2 <= 7'h00;
			p3 <= 7'h00;
			p4 <= 7'h00;
			base_pointer_next <= 12'h000;
			visible_next = 1'b1;
			cur_x_next = 7'h00;
			cur_y_next = 7'h00;
		end
	else
		begin
			do_bell = 1'b0;
			write_e = 1'b0;
			state_next = state_reg;
			cur_x_next = cur_x_reg;
			cur_y_next = cur_y_reg;
			visible_next = visible_reg;
			rd_uart_int <= 1'b0;
			pos_next = pos;
			base_pointer_next <= base_pointer;
			case (state_reg)
				get_ascii_code:  // wait for a new ascii code to process
					if (ascii_ready)
						begin			
							if (ascii_code > 8'h1f && ascii_code < 8'h7f)	
								begin	
									state_next = save_ascii_to_ram_cfg;
									current_code <= ascii_code;
								end
							else if (ascii_code==BELL)
								begin
									rd_uart_int <= 1'b1;	
									do_bell = 1'b1;	
									state_next = get_ascii_code;
								end
							else if (ascii_code==BACKSPACE)
								begin
									rd_uart_int <= 1'b1;	
									state_next = do_backspace;
								end
							else if (ascii_code==TAB)
								begin
									rd_uart_int <= 1'b1;	
									state_next = do_tab;
								end
							else if (ascii_code==LF)
								begin
									rd_uart_int <= 1'b1;	
									state_next = do_lf;				
								end
							else if (ascii_code==CR)
								begin
									rd_uart_int <= 1'b1;	
									state_next = do_cr;
								end
							else if (ascii_code==ESC)
								begin
									rd_uart_int <= 1'b1;	
									state_next = do_esc;
								end
					end
				save_ascii_to_ram_cfg:  // save ascii to ram address setup
					begin
						address <= {cur_y_reg, cur_x_reg};
						state_next = save_ascii_to_ram;
					end
				save_ascii_to_ram:	//save ascii value in ram
					begin
						write_e = 1'b1;
						if ( cur_x_reg + 1 < MAX_X)
							begin
								cur_x_next = cur_x_reg +1;
							end
						else
							begin
								cur_x_next = 0;
								if (cur_y_reg + 1 < MAX_Y)
									begin										
										cur_y_next = cur_y_reg + 1;
									end
								else
									begin
										base_pointer_next <= base_pointer + 7'b1010000;										
									end								
							end
						state_next = do_tidy;
					end
				do_backspace: // do a backspace command
					begin
						if(cur_x_reg > 7'b0000000)
							begin
								cur_x_next = cur_x_reg - 1;
							end
//						else
//							begin							
//								cur_x_next = (cur_y_reg - 1 > 0) ? MAX_X -1 : 7'h00;
//								cur_y_next = (cur_y_reg - 1 > 0) ? cur_y_reg - 1 : 7'h00;
//							end
						state_next = get_ascii_code;
					end				
				do_tab:  //NOT IMPLEMENTED
					begin
						state_next = get_ascii_code;
					end
				do_lf:  //do lf
					begin
						cur_y_next = (cur_y_reg + 1 < MAX_Y) ? cur_y_reg + 1 : 0;
						state_next = get_ascii_code;
//					if (cur_y_reg + 1 < MAX_Y)
//						begin
//							cur_y_next = cur_y_reg + 1;
//							state_next = get_ascii_code;
//						end
//					else
//						begin
//							base_pointer_next <= base_pointer + 7'b1010000;
//							pos_next = base_pointer;
//							current_code <= 7'h20;
//							state_next = clear_old_line;
//						end
					end
				do_cr:  //do cr
					begin
						cur_x_next = 0;						
						state_next = get_ascii_code;
					end
				do_esc: //get bracket part of esc code
					if(ascii_ready)
						begin
							rd_uart_int <= 1'b1;
							if(ascii_code==BRACKET)								
								state_next=get_p1;
							else
								state_next=get_ascii_code;		
						end
				get_p1: //get first character after [
					if(ascii_ready==1'b1)
						begin
							rd_uart_int <= 1'b1;
							case (ascii_code)
								7'h48: 	//H
									begin
										state_next = move_top_left;
									end
								7'h3f:	//?
									begin
										state_next = get_2;
									end
								default:
									begin
										if (ascii_code > 7'h2f && ascii_code < 7'h3a)
											begin
												p1 <= ascii_code;
												state_next = get_p2;												
											end
										else
											state_next = get_ascii_code;
									end
							endcase
						end
				get_p2: // get 2nd character after [
					if(ascii_ready==1'b1)
						begin
							rd_uart_int <= 1'b1;
							case (ascii_code)
								7'h4a: 	//J
									begin
										if(p1==7'h32)
											begin
												state_next = clear_screen;
												pos_next = 0;
												current_code <= 7'h20;
											end
										else
											state_next = get_ascii_code;
									end
								default:
									begin
										if (ascii_code > 7'h2f && ascii_code < 7'h3a)
											begin
												p2 <= ascii_code;
												state_next = get_seperator;												
											end
										else
											state_next = get_ascii_code;
									end
							endcase
						end
				get_p3: // get 1st digit of 2nd param
					if(ascii_ready==1'b1)
						begin
							rd_uart_int <= 1'b1;
							if (ascii_code > 7'h2f && ascii_code < 7'h3a)
								begin
									p3 <= ascii_code;
									state_next = get_p4;
								end
							else
								state_next = get_ascii_code;
						end
				get_p4: // get 2nd digit of 2nd param
					if(ascii_ready==1'b1)
						begin
							rd_uart_int <= 1'b1;
							if (ascii_code > 7'h2f && ascii_code < 7'h3a)
								begin
									p4 <= ascii_code;
									state_next = get_H;
								end
							else
								state_next = get_ascii_code;
						end
				get_seperator: // get the seperator between param's
						if (ascii_ready)
							begin
								rd_uart_int <= 1'b1;
								if(ascii_code==7'h3b)
									state_next = get_p3;
								else
									state_next = get_ascii_code;
							end	
				clear_screen:  // clear the ram
					begin
						if (pos < 12'hfff)
							begin
								write_e = 1'b1;
								address <= pos;
								pos_next = pos + 1;
							end
						else
							state_next = get_ascii_code;
					end
				move_top_left: // move cursor to top left of screen
					begin
						cur_x_next = 7'h00;
						cur_y_next = 7'h00;
						state_next = get_ascii_code;
					end
				get_2:		// get a numerical 2 	
					if(ascii_ready)
						begin								
							rd_uart_int <= 1'b1;
							if (ascii_code==7'h32)
								state_next = get_5;
							else
								state_next = get_ascii_code;							
						end
				get_5:  // get a numerical 5
					if(ascii_ready)
						begin
							rd_uart_int <= 1'b1;
							if (ascii_code==7'h35)
								state_next = get_visible;
							else
								state_next = get_ascii_code;									
						end
				get_H: // get H
						if(ascii_ready)
							begin
								rd_uart_int <= 1'b1;
								if (ascii_code==7'h48) //H
									state_next = move_location;
								else 
									state_next = get_ascii_code;
							end
				get_visible:	//get the last character in the cursor visible/invisible esc codes and change cursor setting			
						if(ascii_ready)
							begin
								rd_uart_int <= 1'b1;
								if	(ascii_code==7'h6c)
									begin
										visible_next = 1'b0;
										state_next = get_ascii_code;
									end
								else if(ascii_code==7'h68)
									begin
										visible_next = 1'b1;
										state_next = get_ascii_code;
									end
								else
									state_next = get_ascii_code;									
							end
				move_location: // move the location of the cursor, params greater than maximum are clamped to the maximum value
					begin
						state_next = get_ascii_code;
						case (p1)
							8'h30: cur_y_next = (p2-8'h30);
							8'h31: cur_y_next = (p2-8'h30) + 4'b1010; // 10
							8'h32: cur_y_next = (p2-8'h30) + 5'b10100; //20
							default: cur_y_next = MAX_Y-1; //30 or greater
						endcase
						case (p3)
							8'h30: cur_x_next = (p4-8'h30);
							8'h31: cur_x_next = (p4-8'h30) + 4'b1010; // 10
							8'h32: cur_x_next = (p4-8'h30) + 5'b10100; //20
							8'h33: cur_x_next = (p4-8'h30) + 5'b11110; //30
							8'h34: cur_x_next = (p4-8'h30) + 6'b101000; //40
							8'h35: cur_x_next = (p4-8'h30) + 6'b110010; //50
							8'h36: cur_x_next = (p4-8'h30) + 6'b111100; //60
							8'h37: cur_x_next = (p4-8'h30) + 7'b1000110; //70
							default: cur_x_next = MAX_X-1; //80 or greater				
						endcase
					end
				do_reset: // reset screen
					begin
						pos_next = 12'h000;
						current_code <= 7'h20;
						cur_x_next = 7'h00;
						cur_y_next = 7'h00;
						visible_next = 1'b1;
						state_next = clear_screen;
					end
					
				do_tidy: //clear the read data and change back to get_ascii state
					begin						
						rd_uart_int <= 1'b1;
						state_next = get_ascii_code;
					end
//				clear_old_line : // clear previous line from scrolling up
//					begin
//						if (pos != base_pointer)
//							begin
//								write_e = 1'b1;
//								address <= pos;
//								pos_next = pos + 1;
//							end
//						else
//							state_next = get_ascii_code;
//					end
			endcase
		end

endmodule
