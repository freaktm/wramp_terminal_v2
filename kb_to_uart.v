//module to read input from keyboard on ps/2 port and send out over a serial port.
//recieve of serial port is sent to parent module for vga display
module kb_controller
   (
    input wire clk, reset,
    input wire ps2d, ps2c,
    output wire tx,
	 input wire rx,
	 output wire ascii_not_ready,
	 output wire [7:0] ascii,
	 input wire rd_uart
   );

   // signal declaration
   wire [7:0] key_code, ascii_code;
   wire kb_not_empty, kb_buf_empty;
	wire use_caps;
	wire shift_on;

   // instantiate keyboard scan code circuit
   kb_code kb_code_unit
      (.clk(clk), .reset(reset), .ps2d(ps2d), .ps2c(ps2c),
       .rd_key_code(kb_not_empty), .key_code(key_code),
       .kb_buf_empty(kb_buf_empty),
		 .use_caps(use_caps),
		 .shift_on(shift_on));

   // instantiate UART
   uart uart_unit
      (.clk(clk), .reset(reset), .rd_uart(rd_uart),
       .wr_uart(kb_not_empty), .rx(rx), .w_data(ascii_code),
       .tx_full(), .rx_empty(ascii_not_ready), .r_data(ascii), .tx(tx));

   // instantiate key-to-ascii code conversion circuit
   key2ascii k2a_unit
      (.key_code(key_code), .ascii_code(ascii_code), .use_caps(use_caps), .shift_on(shift_on));

   assign kb_not_empty = ~kb_buf_empty;
	

endmodule