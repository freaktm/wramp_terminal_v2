module screen_controller
   (
    input wire clk, reset,
    input wire video_on,
    input wire [6:0] ascii,
	 input wire ascii_ready,
    input wire [9:0] pixel_x, pixel_y,
    output reg text_bit,
	 output wire bell,
	 output wire rd_uart
   );
	
   // ROM signals
   wire [10:0] rom_addr;
   wire [6:0] char_addr;
   wire [3:0] row_addr;
   wire [2:0] bit_addr;
   wire [7:0] font_word;
	wire font_bit;
	
   // RAM signals
   wire write_enable;
   wire [11:0] addr_r, addr_w;
   wire [6:0] din, dout;
	wire [6:0] douta;
	wire [11:0] top_pointer;
	wire [11:0] real_address;
	assign real_address = top_pointer + addr_r;
	
   // display cursor
	wire [6:0] cur_x, cur_y;
	wire cursor_on;
	wire visible;
	
	
	////////////////////
	///  VGA STUFF
	////////////////////
	
   // delayed pixel count
   reg [9:0] pix_x1_reg = 9'b000000000;
	reg [9:0] pix_y1_reg = 9'b000000000;
   reg [9:0] pix_x2_reg = 9'b000000000;
	reg [9:0] pix_y2_reg = 9'b000000000;
	
   // vga output signals
   wire out_bit, out_rev_bit;
	
	
	


	//////////////////////
	/// MODULES
	//////////////////////


   // font ROM
   font_rom character_rom
      (.clka(clk), .addra(rom_addr), .douta(font_word));
		

   // dual-port video RAM (2^12-by-7)
   vga_ram vga_ram
      (.clka(clk), .clkb(clk), .wea(write_enable), .web(1'b0), .addra(addr_w), .addrb(real_address),
       .dina(din), .dinb(8'h00), .doutb(dout), .douta(douta), .rsta(1'b0));
			 
	//ascii input module
	input_controller input_controller(
	.clk(clk),
	.reset(reset),
	.ascii_ready(ascii_ready),
	.ascii_code(ascii),
	.write_data(din),	
	.we(write_enable),
	.write_addr(addr_w),
	.read_data(douta),
	.cur_x(cur_x),
	.cur_y(cur_y),
	.bell(bell),
	.visible(visible),
	.rd_uart(rd_uart),
	.top_pointer(top_pointer));
		 
	/////////////////////////
	/// BEHAVIOURAL
	/////////////////////////

   // registers
   always @(posedge clk, posedge reset)
		if (reset)
			begin
				pix_x1_reg <= 0;
				pix_x2_reg <= 0;
				pix_y1_reg <= 0;
				pix_y2_reg <= 0;			
			end
		else
			begin
				pix_x1_reg <= pixel_x;
				pix_x2_reg <= pix_x1_reg;
				pix_y1_reg <= pixel_y;
				pix_y2_reg <= pix_y1_reg;
			end

   // RAM read
   assign addr_r = {pixel_y[8:4], pixel_x[9:3]};
   assign char_addr = dout;
	
   // ROM
   assign row_addr = pixel_y[3:0];
   assign rom_addr = {char_addr, row_addr};	
	
   // use delayed coordinate to select a bit
   assign bit_addr = pix_x2_reg[2:0];
   assign font_bit = font_word[~bit_addr];
	


	/////////////////////////////////////////////
	///  DISPLAY BIT MUXES
	/////////////////////////////////////////////


   // use delayed coordinates for cursor comparison
   assign cursor_on = (pix_y2_reg[8:4]==cur_y) &&
                      (pix_x2_reg[9:3]==cur_x);	
							 
   // white over black and reversed for cursor_on
   assign out_bit = (font_bit) ? 1'b1 : 1'b0;
   assign out_rev_bit = (font_bit) ? (visible) ? 1'b0 : 1'b1 : (visible) ? 1'b1 : 1'b0;
	

							 
   // output bit mux
   always @*
      if (~video_on)
         text_bit = 1'b0; // blank
      else
         if (cursor_on)
            text_bit = out_rev_bit;
          else
            text_bit = out_bit;
				
				
				
endmodule

